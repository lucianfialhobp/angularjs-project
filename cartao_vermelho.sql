-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02-Out-2014 às 19:21
-- Versão do servidor: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cartao_vermelho`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cartao`
--

CREATE TABLE IF NOT EXISTS `cartao` (
`id_cartao_entregue` int(11) NOT NULL,
  `data_entrega` varchar(255) NOT NULL,
  `numero_cartao` varchar(255) NOT NULL,
  `empresa` int(1) NOT NULL,
  `loja` varchar(255) NOT NULL,
  `matricula_colaborador` varchar(255) NOT NULL,
  `cpf_gerente` varchar(11) NOT NULL,
  `data_insercao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Extraindo dados da tabela `cartao`
--

INSERT INTO `cartao` (`id_cartao_entregue`, `data_entrega`, `numero_cartao`, `empresa`, `loja`, `matricula_colaborador`, `cpf_gerente`, `data_insercao`) VALUES
(1, '2014-10-15', '1321231', 1, '1', '12321312321', 'undefined', '2014-10-01 18:52:26'),
(2, '2014-10-11', '12321321', 2, '2', '12321312321', 'undefined', '2014-10-01 19:18:24'),
(3, '2014-10-11', '12321321', 2, '2', '12321312321', 'undefined', '2014-10-01 19:20:15'),
(4, '2014-10-11', '12321321', 2, '2', '12321312321', 'undefined', '2014-10-01 19:20:37'),
(5, '2014-10-11', '12321321', 2, '2', '12321312321', 'undefined', '2014-10-01 19:20:53'),
(6, '2014-10-18', '12321321312', 1, '3', '12321321312', 'undefined', '2014-10-01 19:22:33'),
(7, '2014-10-15', '12321321', 1, '1', '1232131', 'undefined', '2014-10-01 19:23:52'),
(8, '2014-10-10', '123213', 1, '3', '12321312321', 'undefined', '2014-10-01 19:26:44'),
(9, '2014-10-17', '12321321', 1, '1', '1232131231', 'undefined', '2014-10-01 19:27:06'),
(10, '2014-10-25', '12321321', 2, '2', '12321321', 'undefined', '2014-10-01 19:28:07'),
(11, '2014-10-17', '12321321', 1, '1', '21312312312', 'undefined', '2014-10-01 19:31:20'),
(12, '2014-10-17', '12321321', 1, '1', '21312312312', 'undefined', '2014-10-01 19:41:27'),
(13, '2014-10-17', '12321321', 1, '1', '21312312312', 'undefined', '2014-10-01 21:28:23'),
(14, '2014-10-17', '12321321', 1, '1', '21312312312', 'undefined', '2014-10-01 21:28:25'),
(15, '2014-10-08', '12321', 1, '1', '12321321', 'undefined', '2014-10-01 21:30:44'),
(16, '2014-10-25', '12312312', 1, '1', '12321321', 'undefined', '2014-10-01 21:31:08'),
(17, '2014-10-10', '1232131', 0, '', '', 'undefined', '2014-10-01 21:32:33'),
(18, '2014-10-10', '1232131', 1, '3', '12321312321', 'undefined', '2014-10-01 21:32:37'),
(19, '2014-10-10', '12321321', 1, '1', '12321321321', 'undefined', '2014-10-01 21:33:22'),
(20, '2014-10-25', '12321321', 1, '1', '12321321', 'undefined', '2014-10-01 21:34:32'),
(21, '2014-10-25', '12312312', 1, '1', '12312321', 'undefined', '2014-10-01 21:37:31'),
(22, '2014-10-29', '12321321', 2, '2', '12321321', 'undefined', '2014-10-01 21:38:18'),
(23, '2014-10-16', '12321321', 1, '1', '12321321', 'undefined', '2014-10-01 21:40:42'),
(24, '2014-10-15', '12321321', 1, '1', '1232132131', '', '2014-10-01 21:44:22'),
(25, '2014-10-24', '123213', 2, '2', '12321312', '', '2014-10-01 21:45:56'),
(26, '', '', 0, '', '', '', '2014-10-01 21:47:11'),
(27, '', '', 0, '', '', '', '2014-10-01 21:48:08'),
(28, '', '', 0, '', '', '', '2014-10-01 21:48:22'),
(29, '', '', 0, '', '', '', '2014-10-01 21:48:26'),
(30, '2014-10-17', '12321321', 1, '1', '12321321', '', '2014-10-01 21:50:19'),
(31, '', '', 0, '', '', '', '2014-10-01 21:50:42'),
(32, '', '', 0, '', '', '', '2014-10-01 21:50:52'),
(33, '', '', 0, '', '', '', '2014-10-01 21:51:17'),
(34, '', '', 0, '', '', '', '2014-10-01 21:52:31'),
(35, '', '', 0, '', '', '', '2014-10-01 21:56:54'),
(36, '', '', 0, '', '', '', '2014-10-01 22:07:49'),
(37, '', '', 0, '', '', '', '2014-10-01 22:07:56'),
(38, '', '', 0, '', '', '', '2014-10-01 22:07:57'),
(39, '', '', 0, '', '', '', '2014-10-01 22:07:59'),
(40, '', '', 0, '', '', '', '2014-10-01 22:08:32'),
(41, '', '', 0, '', '', '', '2014-10-01 22:09:35'),
(42, '', '', 0, '', '', '', '2014-10-01 22:11:00'),
(43, '', '', 0, '', '', '', '2014-10-01 22:11:17'),
(44, '', '', 0, '', '', '', '2014-10-01 22:11:57'),
(45, '', '', 0, '', '', '', '2014-10-01 22:12:05'),
(46, '', '', 0, '', '', '', '2014-10-01 22:16:16'),
(47, '', '', 0, '', '', '', '2014-10-02 13:28:33'),
(48, '', '', 0, '', '', '', '2014-10-02 13:28:34'),
(49, '', '', 0, '', '', '', '2014-10-02 13:28:37'),
(50, '', '', 0, '', '', '', '2014-10-02 13:28:37'),
(51, '', '', 0, '', '', '', '2014-10-02 13:30:35'),
(52, '', '', 0, '', '', '', '2014-10-02 13:31:07'),
(53, '', '', 0, '', '', '', '2014-10-02 13:31:38'),
(54, '', '', 0, '', '', '', '2014-10-02 13:32:14'),
(55, '', '', 0, '', '', '', '2014-10-02 13:32:35'),
(56, '', '', 0, '', '', '', '2014-10-02 13:32:48'),
(57, '', '', 0, '', '', '', '2014-10-02 13:33:03'),
(58, '', '', 0, '', '', '', '2014-10-02 13:33:47'),
(59, '', '', 0, '', '', '', '2014-10-02 13:34:28'),
(60, '', '', 0, '', '', '', '2014-10-02 13:34:39'),
(61, '', '', 0, '', '', '', '2014-10-02 13:36:45'),
(62, '', '', 0, '', '', '', '2014-10-02 13:36:51'),
(63, '', '', 0, '', '', '', '2014-10-02 13:37:19'),
(64, '', '', 0, '', '', '', '2014-10-02 13:37:37'),
(65, '', '', 0, '', '', '', '2014-10-02 13:39:45'),
(66, '', '', 0, '', '', '', '2014-10-02 13:41:14'),
(67, '', '', 0, '', '', '', '2014-10-02 13:41:36'),
(68, '', '', 0, '', '', '', '2014-10-02 13:41:47'),
(69, '', '', 0, '', '', '', '2014-10-02 13:57:16'),
(70, '', '', 0, '', '', '', '2014-10-02 13:57:50'),
(71, '', '', 0, '', '', '', '2014-10-02 14:57:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
`id_empresa` int(2) NOT NULL,
  `nome_empresa` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `empresas`
--

INSERT INTO `empresas` (`id_empresa`, `nome_empresa`) VALUES
(1, 'Leader'),
(2, 'Seller');

-- --------------------------------------------------------

--
-- Estrutura da tabela `gerentes`
--

CREATE TABLE IF NOT EXISTS `gerentes` (
`id_gerente` int(11) NOT NULL,
  `cpf` varchar(11) CHARACTER SET utf8 NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8 NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `gerentes`
--

INSERT INTO `gerentes` (`id_gerente`, `cpf`, `nome`, `admin`) VALUES
(1, '74863535040', 'Maria Joana', 0),
(2, '30754864189', 'Lucian Fialho', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `lojas`
--

CREATE TABLE IF NOT EXISTS `lojas` (
`id_loja` int(11) NOT NULL,
  `id_empresa` int(2) NOT NULL,
  `bairro_loja` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `lojas`
--

INSERT INTO `lojas` (`id_loja`, `id_empresa`, `bairro_loja`) VALUES
(1, 1, 'Catete'),
(2, 2, 'Moema'),
(3, 1, 'São João de Meriti');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartao`
--
ALTER TABLE `cartao`
 ADD PRIMARY KEY (`id_cartao_entregue`);

--
-- Indexes for table `empresas`
--
ALTER TABLE `empresas`
 ADD PRIMARY KEY (`id_empresa`);

--
-- Indexes for table `gerentes`
--
ALTER TABLE `gerentes`
 ADD PRIMARY KEY (`id_gerente`);

--
-- Indexes for table `lojas`
--
ALTER TABLE `lojas`
 ADD PRIMARY KEY (`id_loja`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cartao`
--
ALTER TABLE `cartao`
MODIFY `id_cartao_entregue` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `empresas`
--
ALTER TABLE `empresas`
MODIFY `id_empresa` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gerentes`
--
ALTER TABLE `gerentes`
MODIFY `id_gerente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lojas`
--
ALTER TABLE `lojas`
MODIFY `id_loja` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
