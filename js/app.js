var cartaoVermelho = angular.module('cartaoVermelho',['ngRoute', 'ui.bootstrap']);

cartaoVermelho.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
		when('/home', { templateUrl: 'home.html', controller: 'homeController'}).
		when('/cadastra',{ templateUrl: 'cadastra.html', controller: 'homeController'}).
		when('/administrador', {templateUrl: 'administrador.html', controller: 'homeController'}).
		otherwise({
			redirectTo: '/home'
 	 	});
  	}
]);

// habilita CORS 
cartaoVermelho.config(['$httpProvider', function($httpProvider) {
  $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);



cartaoVermelho.service('statesService', function($http){

	this.comparaCpf = function(cpfGerente, successCallback){
		
		var cpf_gerente = $.param({ "cpf_gerente": cpfGerente });

		$http({
			method: 'POST',
			url:'app/comparaCpf.php',
			data: cpf_gerente,
			headers: {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(successCallback);
	};

	this.adicionaNabase = function(data_entrega, numero_cartao, empresa, loja, matricula_colaborador, cpf_gerente, successCallback){
		var dados_cartao_entregue = $.param({"data_entrega":data_entrega, "numero_cartao": numero_cartao, "empresa": empresa, "loja": loja, "matricula_colaborador": matricula_colaborador, "cpf_gerente": cpf_gerente});
		
		$http({
			method: 'POST',
			url:'app/insereDataCartao.php',
			data: dados_cartao_entregue,
			headers: {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(successCallback);
	};

	this.getEmpresas = function(successCallback){
		$http.get('app/getEmpresas.php').success(successCallback);
	};

	this.getLojas = function(id_empresa, successCallback){
		var id_empresa = $.param({ "id_empresa": id_empresa });

		$http({
			method: 'POST',
			url:'app/getLojas.php',
			data: id_empresa,
			headers: {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(successCallback);
	}
});


// Inicialização
cartaoVermelho.run(function($rootScope,$modal, statesService) {
	statesService.getEmpresas(function(data){
	    $rootScope.empresas = data;
  	});
});


cartaoVermelho.controller('homeController', function homeController($scope, $location, $rootScope, $modal, $log, statesService){
	$scope.verifyCpf = function(){
		var resultado = statesService.comparaCpf($scope.cpf, function(successCallback){
			if(successCallback[0].success === true){
				
				$rootScope.id_gerente 	= successCallback[0].data_gerente.id_gerente;
				$rootScope.nome 		= successCallback[0].data_gerente.nome
				$rootScope.cpf 			= successCallback[0].data_gerente.cpf;
				var admin				= successCallback[0].data_gerente.administrador;
				
				if(admin !== false){
					$location.path('administrador');
				}else{
					$location.path('cadastra');
				}
			}
		});
	};

	$scope.populaLojas = function(){
		var resultado = statesService.getLojas($scope.empresa, function(successCallback){
			$rootScope.lojas = successCallback;
		});
	};

	$scope.insereDataCartao = function(){
		var resultado = statesService.adicionaNabase($scope.data_entrega, $scope.cartao_num, $scope.empresa, $scope.loja, $scope.matricula_col, $scope.cpf_gerente, function(successCallback){
			if(successCallback[0].success !== false){
				var modalInstance = $modal.open({
			      	templateUrl: 'myModalContent.html',
			     	controller: 'ModalInstanceCtrl'
			    });
			}
		});
	};
});

cartaoVermelho.controller('ModalInstanceCtrl', function ($scope, $modalInstance, $location) {
	$scope.ok = function () {
    	$modalInstance.close();
    	$location.path("home");
  	};
});