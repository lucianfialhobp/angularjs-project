<?php

class Connection {

	private $user;
	private $password;
	private $host;
	private $db_name;
	private $port;
	private $dbArray;

	public function __construct() {
		$this->user = 'root';
	    $this->port = '3306';  //MySql usualy run on 3306 port
	    $this->password = '';
	    $this->host = 'localhost';
	    $this->db_name = 'cartao_vermelho';
	    $this->dbArray = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
	}	

	public function connect() {
		try{
			$response = new PDO('mysql:host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->db_name, $this->user, $this->password, $this->dbArray);
			// Habilita errors do PDO
			//$response->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $response;
		} catch(PDOException $e) {
			$log = 'ERROR: ' . $e->getMessage();
			file_put_contents('../log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
		}

	}
}