<?php

	require_once('connect/connection.php');


	class Cartao {

		private $conn;

		public function __construct() {
			$this->conn = new Connection();
		}

		public function insereDataCartao($data_entrega,$numero_cartao,$empresa,$loja,$matricula_colaborador,$cpf_gerente){
			$query = 'INSERT INTO cartao (data_entrega, numero_cartao, empresa, loja, matricula_colaborador, cpf_gerente) VALUES (:data_entrega,:numero_cartao,:empresa,:loja,:matricula_colaborador,:cpf_gerente)';
			$stmt = $this->conn->connect()->prepare($query);
			
			$stmt->bindValue(':data_entrega', $data_entrega, PDO::PARAM_STR);
			$stmt->bindValue(':numero_cartao', $numero_cartao, PDO::PARAM_STR);
			$stmt->bindValue(':empresa', $empresa, PDO::PARAM_INT);
			$stmt->bindValue(':loja', $loja, PDO::PARAM_STR);
			$stmt->bindValue(':matricula_colaborador', $matricula_colaborador, PDO::PARAM_STR);
			$stmt->bindValue(':cpf_gerente', $cpf_gerente, PDO::PARAM_STR);
			
			try {
				$existe = $stmt->execute();	
			} catch (Exception $e) {
				$log = 'ERROR: ' . $e->getMessage();
				file_put_contents('../log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);	
			}
			
			$data_return = array();
			if($existe){
				$lsd = array('success' => true, 'msg' => 'Usuario adicionado com sucesso');
				array_push($data_return, $lsd);
			}else{
				$data_return = array('success' => false);
				array_push($data_return, $lsd);
			}

			return json_encode($data_return);
		}
	}