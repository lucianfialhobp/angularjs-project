<?php

	require_once('connect/connection.php');

	class Gerente {

		private $conn;

		public function __construct() {
			$this->conn = new Connection();
		}

		public function verificaCpfGerente($cpf){
			$query = 'SELECT * FROM gerentes WHERE cpf= :data';
			$stmt = $this->conn->connect()->prepare($query);
			$stmt->bindValue(':data', $cpf, PDO::PARAM_STR);
			$existe = $stmt->execute();
			$result = $stmt->rowCount();

			$data = $stmt->fetchAll();

			$data_return = array();

			if($result > 0){
				foreach ($data as $key => $value) {
					$lsd = array('success' => true, 'data_gerente' => array('id_gerente' => $value['id_gerente'], 'cpf' => $value['cpf'], 'nome' => $value['nome']));
					array_push($data_return,$lsd);
				}
			}else{
				$lsd = array('success' => false);
				array_push($data_return,$lsd);
			}

			return json_encode($data_return);

		}

		public function getEmpresas(){
			$query = 'SELECT * FROM empresas';
			$stmt = $this->conn->connect()->prepare($query);
			
			$existe = $stmt->execute();
			$result = $stmt->rowCount();

			$data = $stmt->fetchAll();

			$data_return = array();
			
			if($result > 0){
				foreach ($data as $key => $value) {
					$lsd = array('id_empresa' => $value['id_empresa'], 'nome_empresa' => $value['nome_empresa'], 'administrador' => $value['admin']);
					array_push($data_return,$lsd);
				}
			}else{
				$lsd = array('success' => false);
				array_push($data_return,$lsd);
			}

			return json_encode($data_return);
		}
	}