<?php

	require_once('connect/connection.php');

	class Empresa {

		private $conn;

		public function __construct() {
			$this->conn = new Connection();
		}

		public function getEmpresas(){
			$query = 'SELECT * FROM empresas';
			$stmt = $this->conn->connect()->prepare($query);
			
			$existe = $stmt->execute();
			$result = $stmt->rowCount();

			$data = $stmt->fetchAll();

			$data_return = array();
			
			if($result > 0){
				foreach ($data as $key => $value) {
					$lsd = array('id_empresa' => $value['id_empresa'], 'nome_empresa' => $value['nome_empresa']);
					array_push($data_return,$lsd);
				}
			}else{
				$lsd = array('success' => false);
				array_push($data_return,$lsd);
			}

			return json_encode($data_return);
		}

		public function getLojas($id_empresa){
			$query = 'SELECT * FROM lojas WHERE id_empresa= :data';
			$stmt = $this->conn->connect()->prepare($query);
			$stmt->bindValue(':data', $id_empresa, PDO::PARAM_STR);
			$existe = $stmt->execute();
			$result = $stmt->rowCount();

			$data = $stmt->fetchAll();

			$data_return = array();

			if($result > 0){
				foreach ($data as $key => $value) {
					$lsd = array('id_loja' => $value['id_loja'], 'id_empresa' => $value['id_empresa'], 'bairro_loja' => $value['bairro_loja']);
					array_push($data_return,$lsd);
				}
			}else{
				$lsd = array('success' => false);
				array_push($data_return,$lsd);
			}

			return json_encode($data_return);
		}
	}